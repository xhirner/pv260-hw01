using NUnit.Framework;
using static Acronym.Acronym;

namespace Tests
{
    public class Tests
    {
        [Test]
        public void InvalidInputTest()
        {
            Assert.That(GetAcronym(null), Is.EqualTo(""));
            Assert.That(GetAcronym(null, false, false), Is.EqualTo(""));
        }

        [Test]
        public void EmptyInputTest()
        {
            Assert.That(GetAcronym(""), Is.EqualTo(""));
            Assert.That(GetAcronym("", false, true), Is.EqualTo(""));
        }

        [Test]
        public void ValidInputTest()
        {
            Assert.That(GetAcronym("Don't repeat yourself"), Is.EqualTo("DRY"));
            Assert.That(GetAcronym("Asynchronous Javascript and XML"), Is.EqualTo("AJAX"));
            Assert.That(GetAcronym("Complementary metal-oxide semiconductor"), Is.EqualTo("CMOS"));
        }

        [Test]
        public void ExtendedTest()
        {
            Assert.That(GetAcronym("Union of Soviet Socialist Republics", true, true), Is.EqualTo("USSR"));
            Assert.That(GetAcronym("United States of America", false, true), Is.EqualTo("USA"));
            Assert.That(GetAcronym("Institute of Electrical and Electronics Engineers", true, true), Is.EqualTo("IEEE"));
        }
    }
}