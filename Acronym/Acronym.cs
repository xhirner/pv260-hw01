﻿using System.Linq;

namespace Acronym
{
    public class Acronym
    {
        static readonly string[] Conjunctions = {"for","and","nor","but","or","yet","so"};
        static readonly string[] Prepositions = {
            "aboard","about","above","across","after","against","along","amid","among","anti","around","as",
            "at","before","behind","below","beneath","beside","besides","between","beyond","but","by","concerning",
            "considering","despite","down","during","for","from","in","inside","into","like","minus","near","of",
            "off","on","onto","opposite","outside","over","past","per","plus","regarding","round","since","than",
            "through","to","toward","towards","under","underneath","unlike","until","up","upon","with","within","without"
        }; //List is not complete

        public static string GetAcronym(string input)
        {
            return GetAcronym(input, false, false);
        }

        public static string GetAcronym(string input, bool skipConjunctions, bool skipPrepositions)
        {
            if (input == null || input.Equals("")) { return ""; }
            var parsed = input.Split('-', ' ', '_');
            var acronym = "";
            foreach (var s in parsed)
            {
                if (skipConjunctions && Conjunctions.Contains(s.ToLower())) { continue; }

                if (skipPrepositions && Prepositions.Contains(s.ToLower())) { continue; }

                acronym = acronym + s.ToUpper()[0];
            }
            return acronym;
        }
    }
}
